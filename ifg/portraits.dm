/obj/structure/sign/portrait
    icon = 'icons/ifg/portraits.dmi'
    name = "portrait"
    desc = "A portrait of the glorious assistant."
    icon_state = "portrait"
    var/blesses = 1

/obj/structure/sign/portrait/penot
	desc = "�������� ����&#255;� �������� �� �������� ��������� ������������ ���. �� �� ������� � ��� � �����!"
	icon_state = "penot_1"

/obj/structure/sign/portrait/penot/attackby(var/obj/item/I, mob/living/user, params)
	if(istype(I,/obj/item/extinguisher))
		if(blesses > 0)
			user << "<span class='userdanger'>� ���� ��������&#255;� ���!</span>"
			new /obj/item/reagent_containers/food/drinks/drinkingglass/milky(user.loc)
			blesses--
		else
			user.visible_message("<span class='warning'>� ��� �� ��� ����������, ����� [user] ������� �������. ���� �� ���&#255;� �����.</span>", \
								 "<span class='userdanger'>� ���� �� ������� ���� ��������!</span>")
			playsound(loc, 'sound/effects/sparks1.ogg', 50, 1)
			user.adjustBrainLoss(5)
			user.apply_effect(3)

/obj/structure/sign/portrait/by3dpick
	desc = "����������� ������������� ���� ������� �� ��� � ��������. � ����� ���� ���-�� �����&#255;���� � ���� ������� ������ ����."
	icon_state = "by3dpick_1"

/obj/structure/sign/portrait/by3dpick/attackby(var/obj/item/I, mob/living/user, params)
	if(istype(I,/obj/item/extinguisher))
		if(blesses > 0)
			user << "<span class='userdanger'>� ���� ��������&#255;� ���!</span>"
			new /obj/item/toy/katana(user.loc)
			blesses--
		else
			user.visible_message("<span class='warning'>� ��� �� ��� ����������, ����� [user] ������� �������. ���� �� ���&#255;� �����.</span>", \
								 "<span class='userdanger'>� ���� �� ������� ���� ��������!</span>")
			playsound(loc, 'sound/effects/sparks1.ogg', 50, 1)
			user.adjustBrainLoss(5)
			user.apply_effect(3)

/obj/structure/sign/portrait/ruben_1
	desc = "������� �������� �������. �� ���� ���������� ���� ����������, ������������� �� ���."
	icon_state = "ruben_1"

/obj/structure/sign/portrait/ruben_1/attackby(var/obj/item/I, mob/living/user, params)
	if(istype(I,/obj/item/extinguisher))
		if(blesses > 0)
			user << "<span class='userdanger'>� ���� ��������&#255;� ���!</span>"
			new /obj/item/clothing/head/collectable/kitty(user.loc)
			blesses--
		else
			user.visible_message("<span class='warning'>� ��� �� ��� ����������, ����� [user] ������� �������. ���� �� ���&#255;� �����.</span>", \
								 "<span class='userdanger'>� ���� �� ������� ���� ��������!</span>")
			playsound(loc, 'sound/effects/sparks1.ogg', 50, 1)
			user.adjustBrainLoss(5)
			user.apply_effect(3)

/obj/structure/sign/portrait/rodger
	desc = "�������� ������������ ���� ������ ������� �� ��� � �������. ���� ������� ������� �����, �������� � �����&#255;����� ������� ��������."
	icon_state = "portrait-rodger"

/obj/structure/sign/portrait/rodger/attackby(var/obj/item/I, mob/living/user, params)
	if(istype(I,/obj/item/extinguisher))
		if(blesses > 0)
			user << "<span class='userdanger'>� ���� ��������&#255;� ���!</span>"
			new /obj/item/reagent_containers/food/drinks/drinkingglass/milky(user.loc)
			blesses--
		else
			user.visible_message("<span class='warning'>� ��� �� ��� ����������, ����� [user] ������� �������. ���� �� ���&#255;� �����.</span>", \
								 "<span class='userdanger'>� ���� �� ������� ���� ��������!</span>")
			playsound(loc, 'sound/effects/sparks1.ogg', 50, 1)
			user.adjustBrainLoss(5)
			user.apply_effect(3)

/obj/structure/sign/portrait/ruben
	desc = "����� �&#255;�����! ������ ����� ����� ���&#255;�������� ����������� ������ ���� ������� �������� �������."
	icon_state = "portrait-ruben1"

/obj/structure/sign/portrait/ruben/attackby(var/obj/item/I, mob/living/user, params)
	if(istype(I,/obj/item/extinguisher))
		if(blesses > 0)
			user << "<span class='userdanger'>� ���� ��������&#255;� ���!</span>"
			new /obj/item/clothing/head/collectable/kitty(user.loc)
			blesses--
		else
			user.visible_message("<span class='warning'>� ��� �� ��� ����������, ����� [user] ������� �������. ���� �� ���&#255;� �����.</span>", \
								 "<span class='userdanger'>� ���� �� ������� ���� ��������!</span>")
			playsound(loc, 'sound/effects/sparks1.ogg', 50, 1)
			user.adjustBrainLoss(5)
			user.apply_effect(3)

/obj/structure/sign/portrait/bisher
	desc = "������ ����&#255;� �������� �������, ��������&#255; ��������, ����������&#255; ��&#255;. �� ����� �������� ����� �������, ���������� ���������."
	icon_state = "portrait-bishehlop"

/obj/structure/sign/portrait/bisher/attackby(var/obj/item/I, mob/living/user, params)
	if(istype(I,/obj/item/extinguisher))
		if(blesses > 0)
			user << "<span class='userdanger'>� ���� ��������&#255;� ���!</span>"
			new /obj/item/clothing/glasses/eyepatch(user.loc)
			blesses--
		else
			user.visible_message("<span class='warning'>� ��� �� ��� ����������, ����� [user] ������� �������. ���� �� ���&#255;� �����.</span>", \
								 "<span class='userdanger'>� ���� �� ������� ���� ��������!</span>")
			playsound(loc, 'sound/effects/sparks1.ogg', 50, 1)
			user.adjustBrainLoss(5)
			user.apply_effect(3)

/obj/structure/sign/portrait/bisher/examine(mob/user)
	..()
	user.emote("salute")

/obj/item/reagent_containers/food/drinks/drinkingglass/milky
	list_reagents = list("milk" = 50)
	icon_state = "glass_white"
	name = "glass of milk"
	desc = "A glass full of delicious milk!"